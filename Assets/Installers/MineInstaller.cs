using UnityEngine;
using Zenject;

public class MineInstaller : MonoInstaller
{
  //  public GameObject prefab;

    public override void InstallBindings()
    {
        //   Container.Bind<string>().FromInstance("Hello World!");
        //  Container.Bind<Greeter>().AsSingle().NonLazy();

        /* mv = Container.Bind<MineView>().FromNew().AsSingle();
         mm = Container.Bind<MineModel>().FromNew().AsSingle();
         MineController mc = new MineController(mv, mm);
         Container.BindInstance(mc);
         Container.QueueForInject(mv);
         Container.QueueForInject(mm);
         Container.QueueForInject(mc);*/
        //   Container.Bind<MineView>().AsSingle();
        //   Container.Bind<MineModel>().AsSingle();

        //  Container.Bind(typeof(MineView), typeof(MineModel)).To(typeof(MineView), typeof(MineModel)).AsSingle();

        //  Container.Bind<MineView>().AsSingle();
        // Container.Bind<IInitializable>().To<MineModel>().AsTransient();

        //  Container.Bind<MineModel>().AsTransient();
        //   var mm = new MineModel();
        //  mm.Start();

        //Container.BindInstance(new MineModel());
        //Container.Bind<MineModel>().FromInstance(new MineModel()).AsTransient();

        //  Container.Instantiate(prefab).AsTransient();
        //   Container.InstantiateComponent<MineView>(prefab);
        //   Container.Bind<MineView>().FromComponentInNewPrefab(prefab).AsTransient();
        // Container.Bind<MineView>().FromComponentInNewPrefab(prefab).AsTransient();
        //  Container.Bind<MineView>().FromComponentInNewPrefab(prefab).AsTransient();

        Container.BindInterfacesAndSelfTo<MineModel>().AsSingle();

        Container.Bind<string>().FromInstance("Zenject test").AsSingle();

        //  Container.InstantiateComponent<MineView>(prefab);

        //  Container.Bind<MineController>().AsTransient();

    }
}
/*public class Greeter
{
    public Greeter(string message)
    {
        Debug.Log(message);
    }
}*/