using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MineView : MonoBehaviour
{

    public GameObject GameOver;
    public GameObject Win;
    public MineGame Script;
    SpriteRenderer m_SpriteRenderer;
    int _goldCollected;
    int _goldChance;
    public TextMeshProUGUI UI_Shovels;
    public TextMeshProUGUI UI_Golds;
    string DfltString;
    string _goldString;
    string _shovelsString;
    int _tileStatus;
    int _shovels;
    int _goldMax;
    public int Index;

    public delegate void TileEvents(int index);
    public event TileEvents SetNewStatus;
    public event TileEvents TileClick;
    public delegate void TIleStatusHandler(int index);
    public event TIleStatusHandler UpdateTileStatus;
    public delegate void GameHandler();
    public event GameHandler RestartGame;
    public event GameHandler EndGame;
    public delegate void ItemHandler();
    public event ItemHandler GetGold;
    public event ItemHandler GetShovels;
    public event ItemHandler GetMaxGold;

    public void UpdateShovels(int shovels)
    {
        ChangeText(shovels, "Shovels: ", UI_Shovels);
    }
    public void UpdateGold(int gold, int GoldMax)
    {
        ChangeText(gold, "Gold: ", UI_Golds);
    }
    public void WinGold(int gold, int GoldMax)
    {
        Win.SetActive(true);
        ChangeText(gold, "Gold: ", UI_Golds);
    }
    public void LoseShovels(int shovels)
    {
        GameOver.SetActive(true);
        ChangeText(shovels, "Shovels: ", UI_Shovels);
    }

    public void MadeGold(int index)
    {
        if (Index == index)
        {
            m_SpriteRenderer = GetComponent<SpriteRenderer>();
            m_SpriteRenderer.color = Color.yellow;
            Debug.Log("index: " + index + ", madeGold");
        }
    }
    public void CollectGold(int index)
    {
        if (Index == index)
        {
            m_SpriteRenderer = GetComponent<SpriteRenderer>();
            m_SpriteRenderer.color = Color.green;
            Debug.Log("index: " + index + ", CollectGold");
        }

    }
    public void End(int index)
    {
        if (Index == index)
        {
            m_SpriteRenderer = GetComponent<SpriteRenderer>();
            m_SpriteRenderer.color = Color.black;
            Debug.Log("index: " + index + ", End");
        }
    }

    // Start is called before the first frame update
    public void Start()
    {
        var canvas = GameObject.Find("Canvas");
        UI_Shovels = canvas.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        UI_Golds = canvas.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        GameOver = canvas.transform.Find("GameOver").gameObject;
        Win = canvas.transform.Find("Win").gameObject;
        GetGold?.Invoke();
        GetShovels?.Invoke();
        GetMaxGold?.Invoke();
        UpdateShovels(_shovels);
        UpdateGold(_goldCollected, _goldMax);
        UpdateTileStatus?.Invoke(Index);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EndGame?.Invoke();
        }
    }

    void ChangeText(int itemInt, string itemStr, TextMeshProUGUI UI_string)
    {
        string itemText = itemInt.ToString();
        itemStr = itemStr + itemText;
        UI_string.text = itemStr;
    }

    public void OnMouseDown()
    {
        TileClick?.Invoke(Index);
    }
    public void ReturnTileStatus(int index, int Status)
    {
        if (Index == index)
            _tileStatus = Status;
    }
    public void ReturnGold(int Gold)
    {
        _goldCollected = Gold;
    }
    public void ReturnShovels(int Shovels)
    {
        _shovels = Shovels;
    }
    public void ReturnMaxGold(int Gold)
    {
        _goldMax = Gold;
    }
    public void SetIndex(int ind)
    {
        Index = ind;
    }
}
