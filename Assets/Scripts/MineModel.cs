using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Zenject;

public class MineModel : IInitializable
{
    public GameObject Minegame;
    public MineGame Script;
    public bool IsGold = false;

    public delegate void gghandler();
    public delegate void TileHandler(int index);
    public delegate void MakeGoldTile(int index);
    public delegate void ShovelsHandler(int Shovels);
    public delegate void GoldHandler(int Gold, int MaxGold);
    public delegate void TIleStatusHandler(int index, int Status);
    public event ShovelsHandler LoseShovels;
    public event GoldHandler WinGold;
    public event TileHandler End;
    public event MakeGoldTile MadeGold;
    public event TileHandler CollectGold;
    public event ShovelsHandler UpdateShovels;
    public event GoldHandler UpdateGold;
    public event TIleStatusHandler ReturnTileStatus;

    public delegate void ItemHandler(int item);
    public event ItemHandler ReturnGold;
    public event ItemHandler ReturnShovels;
    public event ItemHandler ReturnMaxGold;

    public int[,] tileArray;

    public int _tileStatus;
    int _goldRand;
    int _goldMax;
    int _goldChance;
    int _goldCollected;
    int _shovels;

    [SerializeField]
    private MineSettings _mineSettings;

    private MineInventory _mineInventory;

    public void Initialize()
    {
        Start();
    }

    // Start is called before the first frame update
    public void Start()
    {
        _mineSettings = Resources.Load<MineSettings>("MineSettings");
        _tileStatus = _mineSettings.TileLevels();
        _shovels = _mineSettings.Shovels();
        _goldMax = _mineSettings.GoldMax();
        _goldChance = _mineSettings.GoldChance();
        _mineInventory = new MineInventory(_shovels, 0);
        UpdateShovels?.Invoke(_mineInventory.GetShovels());
        UpdateGold?.Invoke(_mineInventory.GetGold(), _goldMax);
    }

    // Update is called once per frame
    public void Update()
    {
    }

    public void SetNewStatus(int index)
    {
        if (tileArray[index, 2] == 0)
        {
            _mineInventory.SetShovels(_mineInventory.GetShovels() - 1);
            if (_mineInventory.GetShovels() == 0)
            {
                LoseShovels?.Invoke(_mineInventory.GetShovels());
            }
            else
            {
                UpdateShovels?.Invoke(_mineInventory.GetShovels());
            }
            tileArray[index, 1] = tileArray[index, 1] - 1;
            Debug.Log("index: " + index + ", setNewStatus");
            MakeTileGold(index);
            if (tileArray[index, 2] == 1)
            {
                MadeGold?.Invoke(index);
            }
        }
        else
        {
            tileArray[index, 2] = 0;
            TakeGold();
            CollectGold?.Invoke(tileArray[index, 0]);
        }
    }
    public void MakeTileGold(int index)
    {
        _goldRand = Random.Range(0, 101);
        if (tileArray[index, 1] > 0)
        {
            if (_goldRand < _goldChance)
            {
                tileArray[index, 2] = 1;
            }
        }
        else End?.Invoke(index);
    }
    public void TakeGold()
    {
        _mineInventory.SetGold(_mineInventory.GetGold() + 1);
        if (_mineInventory.GetGold() == _goldMax)
        {
            WinGold?.Invoke(_mineInventory.GetGold(), _goldMax);
        }
        else
        {
            UpdateGold?.Invoke(_mineInventory.GetGold(), _goldMax);
        }
    }
    public void UpdateTileStatus(int index)
    {
        ReturnTileStatus?.Invoke(index, tileArray[index, 1]);
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
    public void EndGame()
    {
        Application.Quit();
    }
    public void GetGold()
    {
        ReturnGold?.Invoke(_mineInventory.GetGold());
    }
    public void GetShovels()
    {
        ReturnShovels?.Invoke(_mineInventory.GetShovels());
    }
    public void GetMaxGold()
    {
        ReturnMaxGold?.Invoke(_goldMax);
    }
    public void SetIndexes(int noft)
    {
        tileArray = new int[noft, 3];
        for (int i = 0; i < noft; i++)
        {
            tileArray[i, 0] = i;
            tileArray[i, 1] = _tileStatus;
            tileArray[i, 2] = 0;
        }
    }
    public void TileClick(int index)
    {
        if ((_mineInventory.GetShovels() > 0) && (_mineInventory.GetGold() < _goldMax))
        {
            UpdateTileStatus(index);
            if (tileArray[index, 1] > 0)
            {
                SetNewStatus(index);
            }
            else
            {
                End?.Invoke(index);
            }
        }
    }
}
