using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MineSettings", menuName = "MineSettings", order = 51)]
public class MineSettings : ScriptableObject
{
    [SerializeField]
    private int goldMax;
    [SerializeField]
    private int goldChance;
    [SerializeField]
    private int tileLevels;
    [SerializeField]
    private int shovels;

    public int GoldMax()
    {
        return goldMax;
    }
    public int GoldChance()
    {
        return goldChance;
    }
    public int TileLevels()
    {
        return tileLevels;
    }
    public int Shovels()
    {
        return shovels;
    }
}
