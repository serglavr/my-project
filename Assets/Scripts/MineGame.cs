using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using System.Linq;

public class MineGame : MonoBehaviour
{
    public int NumOfTiles;
    public float DistBtwTiles;
    public int TilesPerRow;
    //  public MineView[] MV;
    public List<MineView> ViewList;
    //  public int GoldMax;
    //  public int GoldChance;
    //  public int Shovels;
    //  public int TileLevels;

    public GameObject _TilePrefab;

    private MineController _mineController;

    [Inject]
    string message;

    [Inject]
    MineModel _mineModel;


    // Start is called before the first frame update
    void Start()
    {
        ViewList = new List<MineView>();
        CreateTiles();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void CreateTiles()
    {
        float xOffset = 0.0f;
        float yOffset = 0.0f;

        for (int tilesCreated = 0; tilesCreated < NumOfTiles; tilesCreated++)
        {
            xOffset += DistBtwTiles;
            if (tilesCreated % TilesPerRow == 0)
            {
                yOffset += DistBtwTiles;
                xOffset = 0;
            }
            var tileObject = Instantiate(_TilePrefab, new Vector2(transform.position.x + xOffset, transform.position.y + yOffset), Quaternion.identity);
            var tileView = tileObject.GetComponent<MineView>();
            tileView.SetIndex(tilesCreated);
            ViewList.Add(tileView);
        }
        MineView[] MVarray = ViewList.ToArray();
        _mineController = new MineController(MVarray, _mineModel, message);
        _mineModel.SetIndexes(NumOfTiles);
    }
}