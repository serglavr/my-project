using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MineController
{
    private MineModel _mineModel;

    private MineView[] _mineView;
    private string _message;

    public MineController(MineView[] view, MineModel model, string message)
    {
        _mineView = view;
        _mineModel = model;
        _message = message;

        //Debug.Log(message);

        foreach (MineView v in _mineView)
        {
            v.SetNewStatus += _mineModel.SetNewStatus;
            v.UpdateTileStatus += _mineModel.UpdateTileStatus;
            v.RestartGame += _mineModel.RestartGame;
            v.EndGame += _mineModel.EndGame;
            v.GetGold += _mineModel.GetGold;
            v.GetShovels += _mineModel.GetShovels;
            v.RestartGame += _mineModel.RestartGame;
            v.EndGame += _mineModel.EndGame;
            v.GetMaxGold += _mineModel.GetMaxGold;
            v.TileClick += _mineModel.TileClick;


            _mineModel.End += v.End;
            _mineModel.MadeGold += v.MadeGold;
            _mineModel.CollectGold += v.CollectGold;
            _mineModel.UpdateShovels += v.UpdateShovels;
            _mineModel.UpdateGold += v.UpdateGold;
            _mineModel.ReturnTileStatus += v.ReturnTileStatus;
            _mineModel.ReturnGold += v.ReturnGold;
            _mineModel.ReturnShovels += v.ReturnShovels;
            _mineModel.ReturnMaxGold += v.ReturnMaxGold;
            _mineModel.LoseShovels += v.LoseShovels;
            _mineModel.WinGold += v.WinGold;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
