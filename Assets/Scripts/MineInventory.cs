using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineInventory
{
    public GameObject Minegame;
    public MineGame Script;
    int _shovels;
    int _goldCollected;

    public MineInventory(int Shovels, int Gold)
    {
        _shovels = Shovels;
        _goldCollected = Gold;
    }
    public int GetShovels()
    { return _shovels; }
    public void SetShovels(int Shovels)
    {
        _shovels = Shovels;
    }
    public int GetGold()
    { return _goldCollected; }
    public void SetGold(int Gold)
    {
        _goldCollected = Gold;
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
}
